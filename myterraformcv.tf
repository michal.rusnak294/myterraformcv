provider "aws" {
  region = "us-east-1"
  access_key = "$ACCESS_KEY"
  secret_key = "$SECRET_KEY"
}

resource "aws_security_group" "example" {
  name        = "example-security-group"
  description = "Example security group"
}

resource "aws_subnet" "example" {
  vpc_id                  = aws_vpc.main.id
  cidr_block              = "10.0.1.0/24"
  availability_zone       = "us-east-1a"
}

resource "aws_vpc" "main" {
  cidr_block = "10.0.0.0/16"
}

resource "aws_instance" "ec2-instance-with-k3s" {
  ami           = "ami-0b93ce03dcbcb10f6"
  instance_type = "t2.micro"
  key_name = "key-for-demo"
  vpc_security_group_ids = [aws_security_group.main.id]

  provisioner "remote-exec" {
    inline = [
    "sudo apt-get install curl",
    "curl -sfL https://get.k3s.io | sh -",
    "git clone https://gitlab.com/michal.rusnak294/myterraformcv.git",
    "sudo k3s kubectl apply -f myterraformcv/deployment.yaml",
    "sudo k3s kubectl apply -f myterraformcv/mycv-svc.yaml",
    "dig +short myip.opendns.com @resolver1.opendns.com > ip.txt",
    "sudo k3s kubectl get all -o wide > all.txt",
    "curl -s --user 'api:$API_KEY' https://api.mailgun.net/v3/$DOMAIN/messages -F from='Sender Name <mailgun@sandbox9ed56ccd8ad5456ea48de2ac5696d531.mailgun.org>' -F to='Recipient michal.rusnak294@gmail.com' -F subject='Attached files' -F text='ip.txt and all.txt files are attached' -F attachment=@ip.txt -F attachment=@all.txt",
    ]
  }

  connection {
    type        = "ssh"
    host        = aws_instance.ec2-instance-with-k3s.public_ip
    user        = "ubuntu"
    private_key = file("/home/mobaxterm/key-for-demo")
    timeout     = "4m"
  }
}


resource "aws_security_group" "main" {
  egress {
      cidr_blocks      = [ "0.0.0.0/0", ]
      description      = ""
      from_port        = 0
      ipv6_cidr_blocks = []
      prefix_list_ids  = []
      protocol         = "-1"
      security_groups  = []
      self             = false
      to_port          = 0
    }
 ingress   {
     cidr_blocks      = [ "0.0.0.0/0", ]
     description      = "allow ssh"
     from_port        = 22
     ipv6_cidr_blocks = []
     prefix_list_ids  = []
     protocol         = "tcp"
     security_groups  = []
     self             = false
     to_port          = 22
  }

 ingress {
    description = "allow HTTP"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

 ingress {
    description = "allow HTTPS"
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description = "allow NodePort"
    from_port   = 30336
    to_port     = 30336
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_key_pair" "deployer" {
  key_name = "key-for-demo"
  public_key = "$PUBLIC_KEY"
}


