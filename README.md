- Terraform script deploying t2.micro ec2 instance in aws with k3s one node cluster
- Deployment and Service (deployment.yaml and mycv-svc.yaml) are applied into cluster.
- Deployment is reachable from outside on https://public-ip:NodePort (especially on NodePort 30336) - might add all nodeports later
- After deployment and service are running I will receive status of all running Kubernetes objects in default namespace and Ip address on my email address - used tool Mailgun

